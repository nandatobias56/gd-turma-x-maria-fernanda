programa
{
	
	funcao inicio()
	{
		cadeia nome
		inteiro idade
		real peso
	
		escreva("Nome: ")
		leia(nome)

		escreva("Idade: ")
		leia(idade)

		escreva("Peso: ")
		leia(peso)

		limpa()
		escreva("Olá ", nome, " você tem ", idade, " anos e pesa: ", peso, "kg")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 190; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */