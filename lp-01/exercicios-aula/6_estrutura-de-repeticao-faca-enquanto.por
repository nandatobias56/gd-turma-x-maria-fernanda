programa
{
	
	funcao inicio()
	{
		logico parar
		caracter continuar
	
	 	faca {
			escreva("Bem vindo ao menu!")
			escreva("\nDeseja continuar? ")
			leia(continuar)

			se(continuar == 'n' ou continuar == 'N') {
				parar = falso
			} senao {
				parar = verdadeiro
			}
	 	} enquanto (parar) 
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 47; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */