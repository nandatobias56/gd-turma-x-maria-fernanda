programa
{
	
	funcao inicio()
	{
		real nota1, nota2, media
	
		escreva("Informe a nota 1: ")
		leia(nota1)
		
		escreva("\nInforme a nota 2: ")
		leia(nota2)
		
		media = (nota1 + nota2) / 2
		escreva("\nSua média é: ", media)
		
		se(media >= 6) {
			escreva("\nParabéns você foi aprovado!!")	
		}
 		senao se (media >= 4.5) {
			escreva("\nVocê está de recuperação!!")	
		} senao {
			escreva("\nVocê está reprovado!!")	
		}
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 249; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */