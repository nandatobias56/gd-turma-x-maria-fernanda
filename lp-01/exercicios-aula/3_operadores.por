programa
{
	
	funcao inicio()
	{
		real numero1, numero2, resultado
	
		escreva("Informe o primeiro número: ")
		leia(numero1)
		escreva("Informe o segundo  número: ")
		leia(numero2)

		escreva("\nResultado soma: ", numero1 + numero2)
		escreva("\nResultado subtração: ", numero1 - numero2)
		escreva("\nResultado divisão: ", numero1 / numero2)
		escreva("\nResultado multiplicação: ", numero1 * numero2)
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 204; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */