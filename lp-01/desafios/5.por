programa
{
	funcao inicio()
	{
		real gasto, valor
		escreva("Digite a quantidade em m³ que foi gasta este mês:")
		leia(gasto)
		limpa()

		se (gasto <= 10){
			valor = 22.38
		}
		senao se (gasto <= 20){
			valor = 22.38 + (3.50 * (gasto - 10))
		}
		senao se (gasto <= 30){
		valor = 22.38 + (8.75 * (gasto - 10))
		}
		senao se (gasto <= 50){
		valor = 22.38 + (9.64 * (gasto - 10))
		}
		senao {
			valor = 22.38 + (10.17 * (gasto - 10))
		}

		escreva("O valor a pagar será de: ",valor)
	}

}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 10; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */