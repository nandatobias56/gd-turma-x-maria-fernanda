programa
{
	
	funcao inicio()
	{
		real renda , gastos , pe_meia , saldo , resto
		
		escreva("Informe sua renda mensal: ")
		leia(renda)

		
		escreva("Informe seus gastos: ")
		leia(gastos)
		limpa()

		
		pe_meia = renda * 0.1
		escreva("Deve-se poupar: ", pe_meia)
		
		saldo = renda - pe_meia
		se(saldo < gastos){
			escreva("Limite atingido!!!!")
		}

		
		resto = saldo - gastos
		se(resto > 0){
			escreva("Após tudo pago, e com o dinheiro reservado para o pé de meia,restará " , resto , "reais")
		}senao{
			escreva(", por isso este mês você ficará com uma dívida de ", resto, " reais")
		}
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 268; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */