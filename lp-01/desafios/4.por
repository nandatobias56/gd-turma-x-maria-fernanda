programa
{
	inclua biblioteca Matematica --> mat
	
	funcao inicio()
	{
		real valor_parcelas , valor
		inteiro parcela
		cadeia nome
		
		escreva("Informe seu nome: ")
		leia(nome)

		escreva("Informe o valor: ")
		leia(valor)

		escreva("Informe em quantas vezes deseja dividir: ")
		leia(parcela)

		limpa()
		escreva("Muito obrigado ", nome, "!\n")

		se(parcela < 1){
			escreva("Não é possivel parcelar. ")
		}senao se(parcela == 1){
			valor_parcelas = valor * 0.9
			escreva("Você parcelou sua compra em 1, no valor de ", valor_parcelas)
		}senao se(parcela <= 10){
			valor_parcelas = valor/parcela
			escreva("Você parcelou sua compra em ", parcela, ", no valor de ", valor_parcelas)
		}senao se(parcela <= 12){
			real total_juros
			total_juros = valor * (mat.potencia(1.012, parcela))

			valor_parcelas = total_juros/ parcela
			escreva("Você parcelou sua compra em ", parcela, ", no valor de ", valor_parcelas)
		}senao{
			escreva("Infelizmente não conseguimos dividir em mais de 12x")
		}
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 50; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */