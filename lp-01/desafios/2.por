programa
{
	
	funcao inicio()
	{
		real peso , altura , idade , imc
		
		escreva("Informe seu peso:")
		leia(peso)

		escreva("Informe sua altura:")
		leia(altura)

		imc = peso / (altura * altura )
		limpa()

		se(imc < 18.5){
		escreva("Baixo peso")
		
		
	}senao se (imc <= 24.99){
		escreva(" Peso normal e IMC igual a ", imc)
		
		
	}senao se (imc <= 29.99 ){
		escreva("Sobrepeso e IMC igual a ", imc)
		
		
	}senao se (imc > 30)
		escreva("Obesidade e IMC igual a ", imc)
		
	}
	
 
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 386; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */